var human = {
	noise: "hello world!",
	makeNoise: function() {
		console.log(this.noise);
	}
}

function createUser(height, properties) {
	var newUser = Object.create(human);
	
	Object.assign(newUser, properties, {
		getHeight: function() {
			return height;
		}
	});
	
	return newUser;
}

var user = createUser("1.94 meters", {
	name: "joepie91",
	noise: "hallo!"
});

user.makeNoise(); // what's the output?
console.log(user.getHeight());